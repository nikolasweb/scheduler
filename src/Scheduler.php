<?php

namespace Wp_Scheduler;


use Wp_Scheduler\Core\Component;

class Scheduler extends Component
{
    public function setup()
    {
        add_action('send_email', [$this, 'send_email']);
    }

    public function add_job_to_queue($action, $args = [])
    {
        as_enqueue_async_action($action, $args);
    }

    public function send_email($args)
    {
        file_put_contents(__DIR__ . '/test.txt', json_encode($args) . "\n", FILE_APPEND);
        call_user_func_array('wp_mail', $args);
    }

}