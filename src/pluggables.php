<?php

if ( ! function_exists('wp_mail') && !
    (( ! empty($_REQUEST['action']) && $_REQUEST['action'] === 'as_async_request_queue_runner') || wp_doing_cron())) {
    {
        $debug = [
            ! function_exists('wp_mail'),
            ( ! empty($_REQUEST['action']) && $_REQUEST['action'] === 'as_async_request_queue_runner'),
            ! (( ! empty($_REQUEST['action']) && $_REQUEST['action'] === 'as_async_request_queue_runner') || wp_doing_cron()),
        ];

        function wp_mail($to, $subject, $message, $headers = '', $attachments = array())
        {
            wp_scheduler()->getScheduler()->add_job_to_queue('send_email', [func_get_args()]);
        }
    }
}