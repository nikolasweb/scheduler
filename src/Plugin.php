<?php


namespace Wp_Scheduler;

use Wp_Scheduler\Core\Plugin as PluginBase;

/**
 * Class Plugin
 * @package Wp_Scheduler
 */
class Plugin extends PluginBase
{

    /**
     * Plugin version
     */
    const VERSION = '0.1.0';

    /**
     * Plugin slug name
     */
    const PLUGIN_SLUG = 'wp-scheduler';

    /**
     * Plugin namespace
     */
    const PLUGIN_NAMESPACE = '\Wp_Scheduler';
    /**
     * @var Scheduler
     */
    private $scheduler;

    /**
     * Plugin constructor.
     */
    public function __construct(Scheduler $scheduler)
    {
        $this->scheduler = $scheduler;
        parent::__construct();
    }

    /**
     * @return Scheduler
     */
    public function getScheduler(): Scheduler
    {
        return $this->scheduler;
    }


    /**
     * Method to check if plugin has its dependencies. If not, it silently aborts
     * @return bool
     */
    protected function get_dependancies_exist()
    {
        return true;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    protected function load_components()
    {
        // Conditionally lazy load components with $this->load()
        return true;
    }

    /**
     * @return bool
     */
    public function setup()
    {
        if (isset($_GET['test'])) {
            $result = wp_mail('info@vaclavgreif.cz','Pepa','Vomáčka');
        }
        return true;
    }



    /**
     * Plugin activation and upgrade
     *
     * @param $network_wide
     *
     * @return void
     */
    public function activate($network_wide)
    {
    }

    /**
     * Plugin de-activation
     *
     * @param $network_wide
     *
     * @return void
     */
    public function deactivate($network_wide)
    {
    }

    /**
     * Plugin uninstall
     * @return void
     */
    public function uninstall()
    {
    }
}
