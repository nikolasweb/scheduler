<?php


use ComposePress\Core\Abstracts\Component_0_9_0_0;

class ComponentMock extends Component_0_9_0_0 {

	protected $lazy_component = 'ComponentChildMock';
	protected $lazy_component_bad;
	protected $lazy_component_bad_class = 'missing';
	protected $lazy_component_bad_stdclass;
	protected $lazy_component_many = [ 'ComponentChildMock', 'ComponentChildMock' ];
	protected $lazy_component_many_bad = [ 'ComponentChildMock', '\stdClass', false ];
	protected $lazy_component_many_bad_empty = [];
	protected $lazy_component_many_bad_class = [ 'ComponentChildMock', 'missing' ];
	private $child;
	private $child2;

	public function __construct(ComponentChildMock $child, ComponentChildMock $child2) {
		$this->lazy_component_bad_stdclass = new stdClass();
		$this->child                       = $child;
		$this->child2                      = $child2;
	}


	/**
	 * @return \ComponentChildMock
	 */
	public function get_child() {
		return $this->child;
	}

	public function is_component( $component, $use_cache = true ) {
		return parent::is_component( $component, $use_cache );
	}

	public function load( $component, ...$args ) {
		return parent::load( $component, ...$args );
	}

	public function is_loaded( $component ) {
		return parent::is_loaded( $component );
	}

	/**
	 * @return string
	 */
	public function get_lazy_component() {
		return $this->lazy_component;
	}

	/**
	 * @return mixed
	 */
	public function get_lazy_component_bad() {
		return $this->lazy_component_bad;
	}

	/**
	 * @return string
	 */
	public function get_lazy_component_bad_class() {
		return $this->lazy_component_bad_class;
	}

	/**
	 * @return \stdClass
	 */
	public function get_lazy_component_bad_stdclass() {
		return $this->lazy_component_bad_stdclass;
	}

	/**
	 * @return array
	 */
	public function get_lazy_component_many_bad() {
		return $this->lazy_component_many_bad;
	}

	/**
	 * @return array
	 */
	public function get_lazy_component_many_bad_empty() {
		return $this->lazy_component_many_bad_empty;
	}

	/**
	 * @return array
	 */
	public function get_lazy_component_many_bad_class() {
		return $this->lazy_component_many_bad_class;
	}

	/**
	 * @return array
	 */
	public function get_lazy_component_many() {
		return $this->lazy_component_many;
	}
}
