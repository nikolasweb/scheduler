<?php

use ComposePress\Core\Abstracts\Component_0_9_0_0;

/**
 * Class ComponentChildFailMock
 */
class ComponentChildFailMock extends Component_0_9_0_0 {

	/**
	 * @return bool
	 */
	public function setup() {
		return false;
	}
}
