<?php


use ComposePress\Core\Abstracts\Manager_0_9_0_0;

class ManagerMockModuleFail extends Manager_0_9_0_0 {
	protected $modules = [
		'\ComponentChildFailMock',
	];
}
