<?php


use ComposePress\Core\Abstracts\Component_0_9_0_0;

class ComponentFailLoadMock extends Component_0_9_0_0 {

	private $child = 'fakeclass';

	/**
	 * @return \fakeclass
	 */
	public function get_child() {
		return $this->child;
	}

	protected function load_components() {
		$this->load( 'child' );
	}
}
