<?php

use ComposePress\Core\Abstracts\Component_0_9_0_0;
use ComposePress\Core\Exception\ComponentInitFailure;

/**
 * Class ComponentChildFailMock
 */
class ComponentChildFailExceptionMock extends Component_0_9_0_0 {

	/**
	 * @return \WP_Error
	 */
	public function setup() {
		return new \WP_Error( 'fail' );
	}
}
