<?php


use ComposePress\Core\Abstracts\Component_0_9_0_0;

class ComponentChildrenArrayMock extends Component_0_9_0_0 {

	private $children = [];

	public function __construct() {
		$this->children = [ new ComponentChildFailExceptionMock(), new ComponentChildFailExceptionMock() ];
	}

	/**
	 * @return array
	 */
	public function get_children() {
		return $this->children;
	}
}
